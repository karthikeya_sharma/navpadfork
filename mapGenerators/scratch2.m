
width = 200;
height = 100;

area = width*height;
density = 0.001;

wmap = zeros(height, width);

expected_num_obstacles = round(density*area);
num_obstacles = poissrnd(expected_num_obstacles);

xs = randi(width, [num_obstacles 1]);
ys = randi(height, [num_obstacles 1]);

ix = sub2ind([height, width], ys, xs);

wmap(ix) = 1;

imshow(poissonProcess(200, 100, 0.001, 1));


