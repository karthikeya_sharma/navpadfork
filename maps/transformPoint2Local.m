function [ transformedPoints ] = transformPoint2Local(p,rotationAngle,translation)

R = [cos(rotationAngle) sin(rotationAngle);...
     -sin(rotationAngle) cos(rotationAngle)];
transformedPoints = bsxfun(@minus,p,translation);
transformedPoints = (R*transformedPoints')';

end

