function [ probability ] = probabilitySafeTrajectorySet( setIds,pathList,pathIds,nonAliasedIndices,pathMap,occMap )
%PROBABILITYSAFE Summary of this function goes here
%   Detailed explanation goes here
probability = 0;

for i=1:size(setIds,1)
    probabilityPathIntersection = 0;
    pathInds = nonAliasedIndices(setIds(i)).indices;
    probabilityPathAlone = getSafeProbabilityIndices(pathInds,occMap);
    for j=i+1:size(setIds,1)
        pathInds2 = unique([nonAliasedIndices(setIds(j)).indices;pathInds]);
        probabilityPathIntersection = probabilityPathIntersection + getSafeProbabilityIndices(pathInds2,occMap);
    end
    probability = probability + probabilityPathAlone - probabilityPathIntersection;
end

