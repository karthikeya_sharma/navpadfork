function [mapIds,decTreeMap] = getStateDecisionTree(stateIds,stateMatrix,mapExtrema)
%% filling up the map
global globalPathIds
global globalPathList
global mapResolution
global mapSize
global minMap
global pathMap
global decTreeMap

mapSize = ceil((mapExtrema(2,:) - mapExtrema(1,:))./mapResolution);
minMap = mapExtrema(1,:);
decTreeMap(mapSize(1),mapSize(2),mapSize(3)).Occ = [];
decTreeMap(mapSize(1),mapSize(2),mapSize(3)).nOcc = [];
decTreeMap(mapSize(1),mapSize(2),mapSize(3)).pathsIds = [];
pathIds = stateMatrix(stateIds(1),stateIds(2),stateIds(3)).pathIds;
pathMap(mapSize(1),mapSize(2),mapSize(3)).pathIds = [];

    for i=1:size(pathIds,1)
        pids = [globalPathIds(pathIds(i),1),globalPathIds(pathIds(i),2)];
        IDs = value2Id(globalPathList(pids(1):pids(2),2:4),minMap,mapResolution);
        linearIds = sub2ind(mapSize, IDs(:,1), IDs(:,2), IDs(:,3));
        linearIds = unique(linearIds);
        for j = 1:size(linearIds,1)
            pathMap(linearIds(j)).pathIds = [pathMap(linearIds(j)).pathIds;pathIds(i)];
        end
    end
    
    mapIds = findNextCell(pathIds,1);
end


function mapIds = findNextCell(pathIds,iterationNumber) % these pathIds are actually indexes for globalPathIds
    if(isempty(pathIds))
        mapIds = [];
        return;
    end
    global globalPathIds
    global globalPathList
    global mapResolution
    global mapSize
    global minMap
    global pathMap % generate path map and then you are done.
    global decTreeMap
    global recurNum
    recurNum = recurNum+1
    iterationNumber
    map = zeros(mapSize);
    for i=1:size(pathIds,1)
        pids = [globalPathIds(pathIds(i),1),globalPathIds(pathIds(i),2)];
        IDs = value2Id(globalPathList(pids(1):pids(2),2:4),minMap,mapResolution);
        linearIDs = sub2ind(mapSize, IDs(:,1), IDs(:,2), IDs(:,3));
        linearIDs = unique(linearIDs);
        map(linearIDs) = map(linearIDs) +1;
    end
    [~,mapIds] = sort(map(:),'descend');
    mapIds = mapIds(iterationNumber);
    [mapIds1 mapIds2 mapIds3] = ind2sub(size(map),mapIds);
    mapIds = [mapIds1,mapIds2,mapIds3];
    
    if(map(mapIds(1),mapIds(2),mapIds(3))==0)
        mapIds = [];
        display('ERROR: this should not have happened, check your code');
        return;
    end
    
    %calculate using PathMap
    pathIdsnOcc = pathIds;
    pathIdsOcc = setdiff(pathIds,pathMap(mapIds(1),mapIds(2),mapIds(3)).pathIds);
    %fill up decTree
    decTreeMap(mapIds(1),mapIds(2),mapIds(3)).pathIds = pathIds;
    
    clear pids;
    clear map;
    clear linearIDs;
    clear IDs;
    clear i;
    clear j;
    clear mapIds1;
    clear mapIds2;
    clear mapIds3;
    % some base conditions
    
    %clear all the variable except the ones that are passed
    %we will make global variables a they stay constant
   if(isempty(pathIdsOcc))
       decTreeMap(mapIds(1),mapIds(2),mapIds(3)).Occ = [];
   else
       decTreeMap(mapIds(1),mapIds(2),mapIds(3)).Occ = findNextCell(pathIdsOcc,1);
   end
   
   decTreeMap(mapIds(1),mapIds(2),mapIds(3)).nOcc = findNextCell(pathIdsnOcc,(iterationNumber+1));
end