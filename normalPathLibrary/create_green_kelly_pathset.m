function [ gk_path_set ] = create_green_kelly_pathset( path_set, N )
%CREATE_GREEN_KELLY_PATHSET Summary of this function goes here
%   Detailed explanation goes here


gk_path_set = path_set(floor(0.5*(1+end)));

while (length(gk_path_set) < N)
    maxArea = 0;
    trajSelected = 0;
    for i = 1:length(path_set)
        minArea = Inf;
        for j = 1:length(gk_path_set)
           eucl = sqrt( (path_set(i).x - gk_path_set(j).x).^2 + (path_set(i).y - gk_path_set(j).y).^2);
           dstep = sqrt(diff(path_set(i).x).^2+diff(path_set(i).y).^2);
           area = sum(eucl(2:end).*dstep);
           if (area < minArea)
               minArea = area;
           end
        end
        if(minArea > maxArea)
            maxArea = minArea;
            trajSelected = i;
        end
    end
    
    gk_path_set = [gk_path_set path_set(trajSelected)];
end

end

